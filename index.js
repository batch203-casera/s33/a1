// API: https://jsonplaceholder.typicode.com/todos

// 1 GET 
(async () => {
    let result = await fetch("https://jsonplaceholder.typicode.com/todos/", { method: "GET" })
    let json = await result.json();
    let titles = json.map(e => e.title);
    console.log(titles)
}
)();


// 3 GET 1 
(async () => {
    let result = await fetch("https://jsonplaceholder.typicode.com/todos/1", { method: "GET" });
    let json = await result.json();
    console.log(json);
    console.log(`The item "${json.title}" on the list has a status of ${json.completed}`);
}
)();

// 4 POST 
fetch("https://jsonplaceholder.typicode.com/todos",
    {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            completed: false,
            id: 201,
            title: "Created To Do List item",
            userId: 1
        })
    }
).then(res => res.json()).then(res => console.log(res));

// 5 PUT
fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            dateCompleted: "pending",
            description: "To update the my to do list with a different data structure",
            id: 1,
            status: "pending",
            title: "Created To Do List item",
            userId: 1
        })
    }
).then(res => res.json()).then(res => console.log(res));

// 6 PATCH 
fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            completed: false,
            dateCompleted: "07/09/21",
            id: 1,
            status: "Complete",
            title: "delectus aut autem",
            userId: 1
        })
    }
).then(res => res.json()).then(res => console.log(res));

// 7 DELETE
(async () => {
    let result = await fetch("https://jsonplaceholder.typicode.com/todos/1", { method: "DELETE" });
    let json = await result.json();
    console.log("DELETED todos/1; Current todos/1: ", json);
}
)();